use std::str::FromStr;

use aoc::{Input, Parse};

// aoc::parts!(1);
aoc::parts!(2);

#[derive(Debug, PartialEq)]
struct RGBCubeParseError;

#[derive(Debug, PartialEq)]
struct RGBCube {
    blue: u32,
    red: u32,
    green: u32,
}

impl FromStr for RGBCube {
    type Err = RGBCubeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut blue = 0;
        let mut red = 0;
        let mut green = 0;

        for c in s.split(',') {
            match c.trim().split_once(' ').unwrap() {
                (val, color) if color == "green" => green = val.parse().unwrap(),
                (val, color) if color == "red" => red = val.parse().unwrap(),
                (val, color) if color == "blue" => blue = val.parse().unwrap(),
                _ => unreachable!(),
            };
        }

        Ok(Self { blue, red, green })
    }
}

impl RGBCube {
    fn is_possible(&self, red: u32, green: u32, blue: u32) -> bool {
        self.red <= red && self.green <= green && self.blue <= blue
    }
}
#[derive(Debug, PartialEq)]
struct RowParseError;

#[derive(Debug, PartialEq)]
struct Row {
    game_id: u32,
    rounds: Vec<RGBCube>,
}

impl FromStr for Row {
    type Err = RowParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (game, rounds) = s.split_once(':').unwrap();

        let (_, game_id) = game.split_once(' ').unwrap();

        let rounds = rounds
            .trim()
            .split(';')
            .map(|r| RGBCube::from_str(r.trim()).unwrap())
            .collect();

        Ok(Row {
            game_id: game_id.parse().unwrap(),
            rounds,
        })
    }
}

impl Row {
    fn is_possible(&self, red: u32, green: u32, blue: u32) -> bool {
        self.rounds.iter().all(|r| r.is_possible(red, green, blue))
    }

    fn get_power(&self) -> u32 {
        let (r, g, b) = self.rounds.iter().fold((0, 0, 0), |acc, x| {
            (
                Ord::max(acc.0, x.red),
                Ord::max(acc.1, x.green),
                Ord::max(acc.2, x.blue),
            )
        });

        r * g * b
    }
}

fn part_1(input: Input) -> impl ToString {
    input
        .lines()
        .map(|l| Row::from_str(l).unwrap())
        .filter(|r| r.is_possible(12, 13, 14))
        .map(|r| r.game_id)
        .sum::<u32>()
}

fn part_2(input: Input) -> impl ToString {
    input
        .lines()
        .map(|l| Row::from_str(l).unwrap().get_power())
        .sum::<u32>()
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use crate::{RGBCube, Row};

    #[test]
    fn test_rgb() {
        let s = "10 green, 9 blue, 1 red";

        let r = RGBCube::from_str(s);

        assert_eq!(
            Ok(RGBCube {
                green: 10,
                blue: 9,
                red: 1
            }),
            r
        )
    }

    #[test]
    fn test_row() {
        let s = "Game 13: 5 green, 2 red; 13 blue, 4 green, 4 red; 8 blue, 4 green";

        let e = Ok(Row {
            game_id: 13,
            rounds: vec![
                RGBCube {
                    blue: 0,
                    green: 5,
                    red: 2,
                },
                RGBCube {
                    blue: 13,
                    green: 4,
                    red: 4,
                },
                RGBCube {
                    blue: 8,
                    green: 4,
                    red: 0,
                },
            ],
        });

        let r = Row::from_str(s);

        assert_eq!(e, r);
    }

    #[test]
    fn test_rgb_possible() {
        let i = RGBCube {
            blue: 2,
            green: 12,
            red: 3,
        };

        assert!(i.is_possible(12, 13, 14));
        assert!(!i.is_possible(12, 11, 14));
    }

    #[test]
    fn test_row_possible() {
        let i = Row {
            game_id: 13,
            rounds: vec![
                RGBCube {
                    blue: 0,
                    green: 5,
                    red: 2,
                },
                RGBCube {
                    blue: 13,
                    green: 4,
                    red: 4,
                },
                RGBCube {
                    blue: 8,
                    green: 4,
                    red: 0,
                },
            ],
        };

        assert!(i.is_possible(5, 8, 15));
        assert!(!i.is_possible(4, 3, 4));
    }

    #[test]
    fn test_max() {
        let i = Row {
            game_id: 13,
            rounds: vec![
                RGBCube {
                    blue: 0,
                    green: 5,
                    red: 2,
                },
                RGBCube {
                    blue: 13,
                    green: 4,
                    red: 4,
                },
                RGBCube {
                    blue: 8,
                    green: 4,
                    red: 0,
                },
            ],
        };

        let e = 4 * 5 * 13;

        let r = i.get_power();

        assert_eq!(r, e);
    }
}
