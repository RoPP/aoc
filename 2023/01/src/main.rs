use aoc::Input;

aoc::parts!(2);

fn part_1(input: Input) -> impl ToString {
    input
        .lines()
        .map(|line| {
            let mut a = line.chars().filter_map(|c| c.to_digit(10));
            let b = a.nth(0).unwrap();
            b * 10 + a.nth_back(0).or(Some(b)).unwrap()
        })
        .sum::<u32>()
}

fn search_digit(line: &str) -> u32 {
    let mut first = line.len();
    let mut last = 0;
    let mut v_f = 0;
    let mut v_l = 0;

    for (pat, val) in [
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ] {
        if let Some(pos) = line.find(pat) {
            if pos <= first {
                first = pos;
                v_f = val
            }
        }

        if let Some(pos) = line.rfind(pat) {
            if pos >= last {
                last = pos;
                v_l = val
            }
        }
    }

    if let Some(pos) = line.find(|c: char| c.is_digit(10)) {
        if pos <= first {
            v_f = line.chars().nth(pos).unwrap().to_digit(10).unwrap();
        }
    }
    if let Some(pos) = line.rfind(|c: char| c.is_digit(10)) {
        if pos >= last {
            v_l = line.chars().nth(pos).unwrap().to_digit(10).unwrap();
        }
    }
    
    let r = v_f * 10 + v_l;
    println!("{line} -> {r}");
    r
}

fn part_2(input: Input) -> impl ToString {
    input
        .lines()
        .map(|line| {
            search_digit(line)
        })
        .sum::<u32>()
}
