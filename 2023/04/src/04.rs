use std::str::FromStr;

aoc::parts!(2);

#[derive(Debug, PartialEq)]
struct CardParseError;

#[derive(Debug, PartialEq)]
struct Card {
    id: u32,
    winning_numbers: Vec<u32>,
    numbers: Vec<u32>,
}

impl FromStr for Card {
    type Err = CardParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (card_id, remmaining) = s.split_once(':').unwrap();

        let card_id = card_id.replace("Card", "");
        let id = card_id.trim().parse::<u32>().unwrap();

        let (w, n) = remmaining.split_once('|').unwrap();

        let winning_numbers = w
            .split(' ')
            .filter(|e| !e.is_empty())
            .map(|e| e.trim().parse::<u32>().unwrap())
            .collect();

        let numbers = n
            .split(' ')
            .filter(|e| !e.is_empty())
            .map(|e| e.trim().parse::<u32>().unwrap())
            .collect();

        Ok(Self {
            id,
            winning_numbers,
            numbers,
        })
    }
}

impl Card {
    fn count(&self) -> usize {
        self.numbers.iter().fold(0, |acc, e| {
            if self.winning_numbers.contains(e) {
                let a = acc * 2;
                if a == 0 {
                    1
                } else {
                    a
                }
            } else {
                acc
            }
        })
    }

    fn matching_number(&self) -> usize {
        self.numbers.iter().fold(0, |acc, e| {
            if self.winning_numbers.contains(e) {
                acc + 1
            } else {
                acc
            }
        })
    }
}

fn part_1_str(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|e| Card::from_str(e).unwrap().count())
        .collect()
}

fn part_1(input: aoc::Input) -> impl ToString {
    part_1_str(input.raw()).iter().sum::<usize>()
}

fn part_2_str(input: &str) -> usize {
    let cards: Vec<Card> = input.lines().map(|e| Card::from_str(e).unwrap()).collect();

    fn count2(cards: &Vec<Card>, index: usize) -> usize {
        match cards[index].matching_number() {
            c if c == 0 => 1,
            c => {
                let end = if index + c > cards.len() {
                    cards.len()
                } else {
                    index + 1 + c
                };

                1 + (index + 1..end).map(|i| count2(cards, i)).sum::<usize>()
            }
        }
    }

    let r: Vec<usize> = (0..cards.len()).map(|i| count2(&cards, i)).collect();

    r.iter().sum::<usize>()
}

fn part_2(input: aoc::Input) -> impl ToString {
    part_2_str(input.raw())
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use crate::{part_1_str, part_2_str, Card};

    const INPUT: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn test_parse() {
        let i = "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1";

        let r = Card::from_str(i).unwrap();

        let e = Card {
            id: 3,
            winning_numbers: vec![1, 21, 53, 59, 44],
            numbers: vec![69, 82, 63, 72, 16, 21, 14, 1],
        };

        assert_eq!(r, e)
    }

    #[test]
    fn test_count() {
        let i = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";

        let r = Card::from_str(i).unwrap().count();

        let e = 8;

        assert_eq!(r, e)
    }

    #[test]
    fn test_part_1_str() {
        let r = part_1_str(INPUT).iter().sum::<usize>();

        let e = 13;

        assert_eq!(r, e);
    }

    #[test]
    fn test_part_2_str() {
        let r = part_2_str(INPUT);

        let e = 30;

        assert_eq!(r, e);
    }
}
