use aoc::{Input, Parse};

aoc::parts!(2);

fn find_gear_symbols(s: &str, nb_col: usize) -> Vec<usize> {
    let mut symbols = vec![];
    for (i, s) in s.lines().enumerate() {
        symbols.extend(
            s.match_indices(|c: char| c == '*')
                .map(|(i, _)| i)
                .map(|e| e + i * nb_col),
        );
    }
    symbols
}

fn find_symbols(s: &str, nb_col: usize) -> Vec<usize> {
    let mut symbols = vec![];
    for (i, s) in s.lines().enumerate() {
        symbols.extend(
            s.match_indices(|c: char| !c.is_ascii_digit() && c != '.')
                .map(|(i, _)| i)
                .map(|e| e + i * nb_col),
        );
    }
    symbols
}

fn find_num(s: &str) -> Vec<(usize, usize)> {
    let mut r = vec![];
    let mut i = 0;

    loop {
        while i < s.len() && !s.idx(i).is_ascii_digit() {
            i += 1
        }
        let mut j = i;
        while j < s.len() && s.idx(j).is_ascii_digit() {
            j += 1
        }

        if i < s.len() && s.idx(i).is_ascii_digit() {
            r.push((i, j));
        }

        if j >= s.len() {
            break;
        }

        i = j;
    }

    r
}

fn find_all_numbers(input: &str, nb_col: usize) -> Vec<(usize, usize, usize)> {
    let mut numbers = vec![];
    for (i, s) in input.lines().enumerate() {
        numbers.extend(find_num(s).iter().map(|(start, end)| {
            let st = start + i * nb_col;
            let en = end + i * nb_col;
            (st, en, s[*start..*end].parse::<usize>().unwrap())
        }));
    }
    numbers
}

fn parts(input: &str, nb_col: usize) -> Vec<usize> {
    let mut r = vec![];

    let symbols = find_symbols(input, nb_col);

    for (i, s) in input.lines().enumerate() {
        r.extend(
            find_num(s)
                .iter()
                .filter(|(start, end)| {
                    if start > &0 {
                        if symbols.contains(&(i * nb_col + start - 1)) {
                            return true;
                        };
                    }

                    if symbols.contains(&(i * nb_col + end)) {
                        return true;
                    };

                    if i > 0 {
                        for a in (i - 1) * nb_col + start - 1..(i - 1) * nb_col + end + 1 {
                            if symbols.contains(&a) {
                                return true;
                            }
                        }
                    }

                    for a in ((i + 1) * nb_col + start - 1)..((i + 1) * nb_col + end + 1) {
                        if symbols.contains(&a) {
                            return true;
                        }
                    }

                    false
                })
                .map(|(start, end)| s[*start..*end].parse::<usize>().unwrap()),
        );
    }

    r
}

fn part_1(input: Input) -> impl ToString {
    parts(input.raw(), 140).iter().sum::<usize>()
}

fn gears(input: &str, nb_col: usize) -> Vec<usize> {
    let mut r = vec![];

    let symbols = find_gear_symbols(input, nb_col);
    let numbers = find_all_numbers(input, nb_col);

    for sym in symbols {
        let ns: Vec<usize> = numbers
            .iter()
            .filter(|(s, e, _)| {
                (*s..*e).contains(&(sym - 1))
                    || (*s..*e).contains(&(sym + 1))
                    || (*s..*e).contains(&(sym - 1 - &nb_col))
                    || (*s..*e).contains(&(sym - 1 + &nb_col))
                    || (*s..*e).contains(&(sym - &nb_col))
                    || (*s..*e).contains(&(sym + &nb_col))
                    || (*s..*e).contains(&(sym + 1 - &nb_col))
                    || (*s..*e).contains(&(sym + 1 + &nb_col))
            })
            .map(|(_, _, val)| *val)
            .collect();

        if ns.len() == 2 {
            r.push(ns[0] * ns[1])
        }
    }
    r
}

fn part_2(input: Input) -> impl ToString {
    gears(input.raw(), 140).iter().sum::<usize>()
}

#[cfg(test)]
mod tests {

    use crate::{find_all_numbers, find_gear_symbols, find_symbols, gears, parts};

    const INPUT: &str = "467..114..
...*......
..35..633.
......#...
617*.....2
.....+..58
..592.....
......755.
...@.*....
.664.598..
.55.......
...&586...";
    const NB_COL: usize = 10;
    const NB_COL2: usize = 140;

    const INPUT2: &str = ".....487.599...........411...........................................574..679.136..........................30......255.......432............
....*......*............*..........&586..........................375...@..*....../.....835.............610*........./...............582.....
...833........304...&.862...............203..........922.125...............819.............@....563.....................722..775............
..............+...994..........#.........*..244.457.....*...........867.........829.....469.....#...........................*...............
313.....753.....................596............*................270..../........*........................38.......836..850..914......942*215
........*............10.525.515.......417$........976...........*..............878.613/.......*247.......*........+.........................
.......725.............*......................848*......236....25.........#605............................602.352...+............505#.619...
741........872...........899...........596..........824..*............542......#....893........299&...........*......389.....176.......*....
..........*....691...165....*../.........@...........#..973...........%.....207............435.........296...269...&........*....$..112.....
...707..311.............@.522.561..........470.................152......524.......964*853.....*...........+......541....578...871...........";

    #[test]
    fn test_find_symbols() {
        let r = find_symbols(INPUT, NB_COL);

        let e = vec![
            1 * NB_COL + 3,
            3 * NB_COL + 6,
            4 * NB_COL + 3,
            5 * NB_COL + 5,
            8 * NB_COL + 3,
            8 * NB_COL + 5,
            11 * NB_COL + 3,
        ];

        assert_eq!(r, e)
    }

    #[test]
    fn test_find_gear_symbols() {
        let r = find_gear_symbols(INPUT, NB_COL);

        let e = vec![1 * NB_COL + 3, 4 * NB_COL + 3, 8 * NB_COL + 5];

        assert_eq!(r, e)
    }

    #[test]
    fn test_find_all_numbers() {
        let r = find_all_numbers(INPUT, NB_COL);

        let e = vec![
            (0, 3, 467),
            (5, 8, 114),
            (22, 24, 35),
            (26, 29, 633),
            (40, 43, 617),
            (49, 50, 2),
            (58, 60, 58),
            (62, 65, 592),
            (76, 79, 755),
            (91, 94, 664),
            (95, 98, 598),
            (101, 103, 55),
            (114, 117, 586),
        ];

        assert_eq!(r, e)
    }

    #[test]
    fn test_parts() {
        let r = parts(INPUT, NB_COL);

        let e = vec![467, 35, 633, 617, 592, 755, 664, 598, 55, 586];

        assert_eq!(r, e)
    }

    #[test]
    fn test_parts_2() {
        let r = parts(INPUT2, NB_COL2);

        let e = vec![
            487, 599, 411, 574, 679, 136, 30, 255, 586, 610, 833, 304, 862, 203, 922, 125, 819,
            563, 775, 994, 244, 457, 867, 829, 469, 753, 596, 270, 38, 836, 914, 942, 215, 10, 525,
            417, 976, 878, 613, 247, 725, 848, 236, 25, 605, 602, 352, 505, 619, 872, 899, 596,
            824, 542, 299, 389, 176, 165, 973, 207, 435, 296, 269, 112, 311, 522, 561, 964, 853,
            541, 871,
        ];

        assert_eq!(r, e)
    }

    #[test]
    fn test_gears() {
        let r = gears(INPUT, NB_COL);

        let e = vec![467 * 35, 755 * 598];

        assert_eq!(r, e)
    }
}
