use core::fmt;
use std::{cmp::Ordering, collections::BTreeMap, path::Display, slice::Iter, str::FromStr};

aoc::parts!(2);

#[derive(Debug)]
struct CardParseError;

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord)]
enum Card {
    Jocker, // Part 2
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack, // Part 1
    Queen,
    King,
    Ace,
}

impl FromStr for Card {
    type Err = CardParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "2" => Self::Two,
            "3" => Self::Three,
            "4" => Self::Four,
            "5" => Self::Five,
            "6" => Self::Six,
            "7" => Self::Seven,
            "8" => Self::Eight,
            "9" => Self::Nine,
            "T" => Self::Ten,
            "J" => Self::Jack,
            "*" => Self::Jocker,
            "Q" => Self::Queen,
            "K" => Self::King,
            "A" => Self::Ace,
            _ => unreachable!(),
        })
    }
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let c = match self {
            Self::Two => "2",
            Self::Three => "3",
            Self::Four => "4",
            Self::Five => "5",
            Self::Six => "6",
            Self::Seven => "7",
            Self::Eight => "8",
            Self::Nine => "9",
            Self::Ten => "T",
            Self::Jack => "J",
            Self::Queen => "Q",
            Self::King => "K",
            Self::Ace => "A",
        };
        write!(f, "{}", c)
    }
}

#[derive(Debug, PartialEq, PartialOrd)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct HandParseError;

#[derive(Debug, PartialEq, Eq)]
struct Hand {
    first: Card,
    second: Card,
    third: Card,
    fourth: Card,
    fifth: Card,
}

impl Hand {
    fn new(first: Card, second: Card, third: Card, fourth: Card, fifth: Card) -> Self {
        Self {
            first,
            second,
            third,
            fourth,
            fifth,
        }
    }

    fn get_nb_jocker(&self) -> u32 {
        let mut r = 0;
        for c in [
            &self.first,
            &self.second,
            &self.third,
            &self.fourth,
            &self.fifth,
        ] {
            if c == &Card::Jocker {
                r += 1;
            }
        }

        r
    }

    fn get_type(&self) -> HandType {
        let mut count = BTreeMap::new();

        for card in [
            &self.first,
            &self.second,
            &self.third,
            &self.fourth,
            &self.fifth,
        ] {
            *count.entry(card).or_insert(0) += 1;
        }

        let c: Vec<u32> = count.values().cloned().collect();

        match c.len() {
            5 => match self.get_nb_jocker() {
                1 => HandType::OnePair,
                0 => HandType::HighCard,
                _ => unreachable!(),
            },
            4 => match self.get_nb_jocker() {
                1 | 2 => HandType::ThreeOfAKind,
                0 => HandType::OnePair,
                _ => unreachable!(),
            },
            3 => {
                if c.contains(&3) {
                    match self.get_nb_jocker() {
                        1 | 3 => HandType::FourOfAKind,
                        0 => HandType::ThreeOfAKind,
                        _ => unreachable!(),
                    }
                } else {
                    match self.get_nb_jocker() {
                        2 => HandType::FourOfAKind,
                        1 => HandType::FullHouse,
                        0 => HandType::TwoPair,
                        _ => unreachable!(),
                    }
                }
            }
            2 => {
                if c.contains(&3) {
                    match self.get_nb_jocker() {
                        2 | 3 => HandType::FiveOfAKind,
                        0 => HandType::FullHouse,
                        _ => unreachable!(),
                    }
                } else {
                    match self.get_nb_jocker() {
                        1 | 4 => HandType::FiveOfAKind,
                        0 => HandType::FourOfAKind,
                        _ => unreachable!(),
                    }
                }
            }
            1 => HandType::FiveOfAKind,
            _ => unreachable!(),
        }
    }
}

impl FromStr for Hand {
    type Err = HandParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            first: Card::from_str(s.get(0..1).unwrap()).unwrap(),
            second: Card::from_str(s.get(1..2).unwrap()).unwrap(),
            third: Card::from_str(s.get(2..3).unwrap()).unwrap(),
            fourth: Card::from_str(s.get(3..4).unwrap()).unwrap(),
            fifth: Card::from_str(s.get(4..5).unwrap()).unwrap(),
        })
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }

    fn ge(&self, other: &Self) -> bool {
        self.get_type() >= other.get_type()
    }

    fn gt(&self, other: &Self) -> bool {
        self.get_type() > other.get_type()
    }

    fn lt(&self, other: &Self) -> bool {
        self.get_type() < other.get_type()
    }

    fn le(&self, other: &Self) -> bool {
        self.get_type() <= other.get_type()
    }
}

impl fmt::Display for Hand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}{}{}{}",
            self.first, self.second, self.third, self.fourth, self.fifth
        )
    }
}
impl Ord for Hand {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self.get_type() > other.get_type() {
            Ordering::Greater
        } else if self.get_type() == other.get_type() {
            if self.first != other.first {
                if self.first > other.first {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            } else if self.second != other.second {
                if self.second > other.second {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            } else if self.third != other.third {
                if self.third > other.third {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            } else if self.fourth != other.fourth {
                if self.fourth > other.fourth {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            } else if self.fifth > other.fifth {
                Ordering::Greater
            } else {
                Ordering::Less
            }
        } else {
            Ordering::Less
        }
    }
}

#[derive(Debug)]
struct HandAndBidParseError;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct HandAndBid {
    hand: Hand,
    bid: u32,
}

impl FromStr for HandAndBid {
    type Err = HandAndBidParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (cards, bid) = s.split_once(' ').unwrap();

        let hand = Hand::from_str(cards).unwrap();

        Ok(Self {
            hand,
            bid: bid.parse::<u32>().unwrap(),
        })
    }
}

impl HandAndBid {
    fn new(hand: Hand, bid: u32) -> HandAndBid {
        HandAndBid { hand, bid }
    }
}

fn parse_input(input: &str) -> Vec<HandAndBid> {
    input
        .lines()
        .map(|e| HandAndBid::from_str(e).unwrap())
        .collect()
}

fn part_1(input: aoc::Input) -> impl ToString {
    let mut hb = parse_input(input.raw());
    hb.sort();

    hb.iter()
        .enumerate()
        .fold(0, |acc, (i, v)| acc + (i as u32 + 1u32) * v.bid)
}

fn part_2(input: aoc::Input) -> impl ToString {
    let mut hb = parse_input(&input.raw().replace("J", "*"));
    hb.sort();

    hb.iter()
        .enumerate()
        .fold(0, |acc, (i, v)| acc + (i as u32 + 1u32) * v.bid)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

    #[test]
    fn test_parse() {
        let r: Vec<HandAndBid> = parse_input(INPUT);

        let e = vec![
            HandAndBid::new(
                Hand::new(Card::Three, Card::Two, Card::Ten, Card::Three, Card::King),
                765,
            ),
            HandAndBid::new(
                Hand::new(Card::Ten, Card::Five, Card::Five, Card::Jack, Card::Five),
                684,
            ),
            HandAndBid::new(
                Hand::new(Card::King, Card::King, Card::Six, Card::Seven, Card::Seven),
                28,
            ),
            HandAndBid::new(
                Hand::new(Card::King, Card::Ten, Card::Jack, Card::Jack, Card::Ten),
                220,
            ),
            HandAndBid::new(
                Hand::new(Card::Queen, Card::Queen, Card::Queen, Card::Jack, Card::Ace),
                483,
            ),
        ];

        assert_eq!(r, e)
    }

    #[test]
    fn test_sort_cards() {
        let mut i = [Card::Jack, Card::King, Card::Ten, Card::Nine, Card::Jack];

        let e = [Card::Nine, Card::Ten, Card::Jack, Card::Jack, Card::King];

        i.sort();

        assert_eq!(i, e)
    }

    #[test]
    fn test_sort_hand() {
        let mut i = [
            Hand::from_str("32T3K"),
            Hand::from_str("23T3K"),
            Hand::from_str("32K3K"),
            Hand::from_str("32TK3"),
            Hand::from_str("3TT3K"),
            Hand::from_str("T55J5"),
            Hand::from_str("KK677"),
            Hand::from_str("KTJJT"),
            Hand::from_str("QQQJA"),
        ];
        let e = [
            Hand::from_str("23T3K"),
            Hand::from_str("32T3K"),
            Hand::from_str("32TK3"),
            Hand::from_str("32K3K"),
            Hand::from_str("3TT3K"),
            Hand::from_str("KTJJT"),
            Hand::from_str("KK677"),
            Hand::from_str("T55J5"),
            Hand::from_str("QQQJA"),
        ];

        i.sort();

        assert_eq!(i, e)
    }

    #[test]
    fn test_sort_hand_with_jocker() {
        let mut i = [
            Hand::from_str("JJJJJ"),
            Hand::from_str("2JJJJ"),
            Hand::from_str("33JJJ"),
            Hand::from_str("444JJ"),
            Hand::from_str("1JJJJ"),
            Hand::from_str("T55JJ"),
            Hand::from_str("KK677"),
            Hand::from_str("KTJJT"),
            Hand::from_str("QQQJA"),
        ];
        let e = [
            Hand::from_str("23T3K"),
            Hand::from_str("32T3K"),
            Hand::from_str("32TK3"),
            Hand::from_str("32K3K"),
            Hand::from_str("3TT3K"),
            Hand::from_str("KTJJT"),
            Hand::from_str("KK677"),
            Hand::from_str("T55J5"),
            Hand::from_str("QQQJA"),
        ];

        i.sort();

        assert_eq!(i, e)
    }

    #[test]
    fn test_part_1() {
        let mut hb = parse_input(INPUT);
        hb.sort();

        let r = hb
            .iter()
            .enumerate()
            .fold(0, |acc, (i, v)| acc + (i as u32 + 1u32) * v.bid);

        let e = 6440;

        assert_eq!(r, e)
    }

    #[test]
    fn test_part_2() {
        let mut hb = parse_input(&INPUT.replace("J", "*"));
        hb.sort();

        let r = hb
            .iter()
            .enumerate()
            .fold(0, |acc, (i, v)| acc + (i as u32 + 1u32) * v.bid);

        let e = 5905;

        assert_eq!(r, e)
    }
}
