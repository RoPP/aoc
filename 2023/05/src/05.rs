#![feature(iter_array_chunks)]

use std::str::FromStr;

use aoc::IterUnwrap;

aoc::parts!(2);

#[derive(Debug, PartialEq)]
struct Mapping {
    dest_start: u32,
    source_start: u32,
    range_length: u32,
}

impl Mapping {
    fn new(dest_start: u32, source_start: u32, range_length: u32) -> Self {
        Self {
            dest_start,
            source_start,
            range_length,
        }
    }

    fn get_out(&self, in_: u32) -> Option<u32> {
        if (self.source_start..self.source_start + self.range_length).contains(&in_) {
            Some(in_ - self.source_start + self.dest_start)
        } else {
            None
        }
    }
}

#[derive(Debug)]
struct MapParseError;

#[derive(Debug, PartialEq)]
struct Map {
    in_ranges: Vec<Mapping>,
}

impl FromStr for Map {
    type Err = MapParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let in_ranges: Vec<Mapping> = s
            .lines()
            .filter(|l| !l.contains("map"))
            .map(|l| {
                let mut iter = l.splitn(3, ' ');
                Mapping::new(
                    iter.next().unwrap().parse::<u32>().unwrap(),
                    iter.next().unwrap().parse::<u32>().unwrap(),
                    iter.next().unwrap().parse::<u32>().unwrap(),
                )
            })
            .collect();

        Ok(Self { in_ranges })
    }
}

impl Map {
    fn get_out(&self, in_: u32) -> u32 {
        for r in &self.in_ranges {
            match r.get_out(in_) {
                Some(v) => return v,
                None => continue,
            }
        }

        in_
    }
}

fn parse_seeds(input: &str) -> Vec<u32> {
    let input = input.replace("seeds: ", "");
    input
        .split(' ')
        .map(|e| e.parse::<u32>().unwrap())
        .collect()
}

fn part_1_str(input: &str) -> u32 {
    let mut iter = input.split("\n\n");

    let seeds = parse_seeds(iter.next_uw());

    let seed_to_soil = Map::from_str(iter.next_uw()).unwrap();
    let soil_to_fertilizer = Map::from_str(iter.next_uw()).unwrap();
    let fertilizer_to_water = Map::from_str(iter.next_uw()).unwrap();
    let water_to_light = Map::from_str(iter.next_uw()).unwrap();
    let light_to_temperature = Map::from_str(iter.next_uw()).unwrap();
    let temperature_to_humidity = Map::from_str(iter.next_uw()).unwrap();
    let humidity_to_location = Map::from_str(iter.next_uw()).unwrap();

    seeds
        .iter()
        .map(|e| seed_to_soil.get_out(*e))
        .map(|e| soil_to_fertilizer.get_out(e))
        .map(|e| fertilizer_to_water.get_out(e))
        .map(|e| water_to_light.get_out(e))
        .map(|e| light_to_temperature.get_out(e))
        .map(|e| temperature_to_humidity.get_out(e))
        .map(|e| humidity_to_location.get_out(e))
        .min()
        .unwrap()
}

fn part_1(input: aoc::Input) -> impl ToString {
    part_1_str(input.raw())
}

fn parse_seeds2(input: &str) -> Vec<(u32, u32)> {
    let input = input.replace("seeds: ", "");
    input
        .split(' ')
        .array_chunks()
        .map(|[s, l]| (s.parse::<u32>().unwrap(), l.parse::<u32>().unwrap()))
        .collect()
}

fn part_2_str(input: &str) -> u32 {
    let mut iter = input.split("\n\n");

    let seeds = parse_seeds2(iter.next_uw());

    let seed_to_soil = Map::from_str(iter.next_uw()).unwrap();
    let soil_to_fertilizer = Map::from_str(iter.next_uw()).unwrap();
    let fertilizer_to_water = Map::from_str(iter.next_uw()).unwrap();
    let water_to_light = Map::from_str(iter.next_uw()).unwrap();
    let light_to_temperature = Map::from_str(iter.next_uw()).unwrap();
    let temperature_to_humidity = Map::from_str(iter.next_uw()).unwrap();
    let humidity_to_location = Map::from_str(iter.next_uw()).unwrap();

    seeds
        .iter()
        .map(|(s, l)| {
            (*s..*s + *l)
                .into_iter()
                .map(|e| seed_to_soil.get_out(e))
                .map(|e| soil_to_fertilizer.get_out(e))
                .map(|e| fertilizer_to_water.get_out(e))
                .map(|e| water_to_light.get_out(e))
                .map(|e| light_to_temperature.get_out(e))
                .map(|e| temperature_to_humidity.get_out(e))
                .map(|e| humidity_to_location.get_out(e))
                .min()
                .unwrap()
        })
        .min()
        .unwrap()
}

fn part_2(input: aoc::Input) -> impl ToString {
    part_2_str(input.raw())
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use crate::{parse_seeds, part_1_str, Map, Mapping, part_2_str};

    const INPUT: &str = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn test_mapping() {
        let m = Mapping::new(34, 54, 23);

        let e = Some(66 - 54 + 34);

        let r = m.get_out(66);

        assert_eq!(r, e);

        let e = None;

        let r = m.get_out(400);

        assert_eq!(r, e)
    }

    #[test]
    fn test_map() {
        let m = Map {
            in_ranges: vec![Mapping::new(50, 10, 20), Mapping::new(88, 35, 10)],
        };

        let e = 90;
        let r = m.get_out(37);

        assert_eq!(r, e);

        let e = 54;
        let r = m.get_out(54);

        assert_eq!(r, e)
    }

    #[test]
    fn test_parse_seeds() {
        let input = "seeds: 79 14 55 13";

        let e = vec![79, 14, 55, 13];

        let r = parse_seeds(input);

        assert_eq!(r, e)
    }

    #[test]
    fn test_parse_map() {
        let input = "temperature-to-humidity map:
0 69 1
1 0 69";

        let e = Map {
            in_ranges: vec![Mapping::new(0, 69, 1), Mapping::new(1, 0, 69)],
        };

        let r = Map::from_str(input).unwrap();

        assert_eq!(r, e)
    }

    #[test]
    fn test_part_1_str() {
        let r = part_1_str(INPUT);

        let e = 35;

        assert_eq!(r, e)
    }

    #[test]
    fn test_part_2_str() {
        let r = part_2_str(INPUT);

        let e = 46;

        assert_eq!(r, e)
    }
}
