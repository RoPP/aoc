aoc::parts!(2);

struct Race {
    time: u64,
    distance: u64,
}
P
impl Race {
    fn new(time: u64, distance: u64) -> Race {
        Race { time, distance }
    }

    fn foo(&self, hold: u64) -> u64 {
        (self.time - hold) * hold
    }

    fn nb_winning(&self) -> u64 {
        (0..self.time - 0)
            .filter(|t| self.foo(*t) > self.distance)
            .count() as u64
    }
}

fn part_1(_input: aoc::Input) -> impl ToString {
    let r = vec![
        Race::new(53, 250),
        Race::new(91, 1330),
        Race::new(67, 1081),
        Race::new(68, 1025),
    ];

    r.iter().map(|r| r.nb_winning()).fold(1, |acc, e| acc * e)
}

fn part_2(_input: aoc::Input) -> impl ToString {
    Race::new(53916768, 250133010811025).nb_winning()
}   

#[cfg(test)]
mod tests {
    use crate::Race;

    #[test]
    fn test_ex_1() {
        let r = vec![Race::new(7, 9), Race::new(15, 40), Race::new(30, 200)];

        let res = r.iter().map(|r| r.nb_winning()).fold(1, |acc, e| acc * e);

        assert_eq!(res, 288)
    }
    
    #[test]
    fn test_ex_2() {
        let r = Race::new(71530, 940200);

        assert_eq!(r.nb_winning(), 71503)
    }
}
