use std::{collections::HashMap, str::FromStr};

aoc::parts!(2);

fn parse_line(s: &str) -> (&str, &str, &str) {
    let (val, left_right) = s.split_once('=').unwrap();

    let (left, right) = left_right.trim().split_once(',').unwrap();

    (
        val.trim(),
        left.get(1..4).unwrap(),
        right.get(1..4).unwrap(),
    )
}

fn parse_map(input: &str) -> HashMap<&str, (&str, &str)> {
    let mut map = HashMap::new();

    for line in input.lines() {
        if !line.contains('=') {
            continue;
        }
        let (v, l, r) = parse_line(line);

        map.insert(v, (l, r));
    }

    map
}

fn part_1_str(input: &str) -> usize {
    let mut iter = input.lines();

    let directions: Vec<char> = iter.next().unwrap().trim().chars().collect();
    let dir_len = directions.len();

    let map = parse_map(input);

    let mut v = "AAA";
    let mut i = 0;

    while v != "ZZZ" {
        let (l, r) = map.get(v).unwrap();

        v = match directions.get(i % dir_len) {
            Some('R') => r,
            Some('L') => l,
            _ => unreachable!(),
        };
        i += 1
    }

    i
}

fn part_1(input: aoc::Input) -> impl ToString {
    part_1_str(input.raw())
}

fn read_side<'a>(dir: char, left: &'a str, right: &'a str) -> &'a str {
    match dir {
        'R' => right,
        'L' => left,
        _ => unreachable!(),
    }
}

fn part_2_str(input: &str) -> usize {
    let mut iter = input.lines();

    let directions: Vec<char> = iter.next().unwrap().trim().chars().collect();
    let dir_len = directions.len();

    let map = parse_map(input);

    let values: Vec<&str> = map
        .keys()
        .filter(|e| e.ends_with('A'))
        .map(|e| *e)
        .collect();

    let mut r = vec![];

    for mut val in values {
        let mut i = 0;
        let mut first = 0;
        let mut next = 0;

        loop {
            let (left, right) = map.get(val).unwrap();
            val = read_side(*directions.get(i % dir_len).unwrap(), left, right);

            if val.ends_with('Z') {
                if first == 0 {
                    first = i + 1;
                } else {
                    next = i + 1;
                    r.push((first, next - first));
                    break;
                }
            }

            i += 1
        }
    }

    let mut max = r.iter().map(|(f, _)| f).max().unwrap();
    while r.iter().map(|(f, _)| f).min().unwrap() != max {
        r = r
            .iter()
            .map(|(f, m)| {
                let mut t = *f;
                while t < *max {
                    t += m;
                }
                (t, *m)
            })
            .collect();
        max = r.iter().map(|(f, _)| f).max().unwrap();
    }

    *max
}

fn part_2(input: aoc::Input) -> impl ToString {
    part_2_str(input.raw())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_binary_tree() {
        let input = "LR

11A = (11B, XXX)
11B = (11E, 11E)
11E = (11C, 11C)
11C = (11Z, 11Z)
11Z = (11I, 11I)
11I = (11G, 11G)
11G = (11D, 11D)
11D = (11H, 11H)
11H = (11E, 11E)
22A = (22B, 22B)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22D, 22D)
22D = (22E, 22E)
22E = (22B, 22B)
33A = (33B, 33B)
33B = (33C, 33C)
33C = (33D, 33D)
33D = (33E, 33E)
33E = (33F, 33F)
33F = (33Z, 33Z)
33G = (33H, 33H)
33H = (33B, 33B)
33Z = (33G, 33G)";

        let r = part_2_str(input);

        assert_eq!(r, 158)
    }

    #[test]
    fn test_part_2() {
        let input = "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

        let r = part_2_str(input);

        assert_eq!(r, 6)
    }
}
